
#include <stdio.h>
#include <stdlib.h>
 
typedef struct Node
{
  struct Node *left;        
  int data;            
  struct Node *right;        
} Node;
 

 Node *Create_Binary_Tree (Node * node, int value)
{
  if (node == NULL)
    {
      Node *first_node = (Node *) malloc (sizeof (Node));
      first_node->left = NULL;
      first_node->right = NULL;
      first_node->data = value;
      
      return first_node;
    }
 
  if (value <= node->data)        
    node->left = Create_Binary_Tree(node->left, value);
  else                
    node->right = Create_Binary_Tree(node->right, value);
  return node;
}

int find_treeHeight (Node * node)
{
int leftHeight = 0;
int rightHeight = 0;
  
 if (node == NULL)
  {
    return 0;
  }
  
  else if (node != NULL)
    {
      leftHeight = find_treeHeight (node->left);
      rightHeight = find_treeHeight (node->right);
      
      if (leftHeight > rightHeight)
      {
          return (leftHeight+1);
      }
      else
      {   
          return (rightHeight+1);
      }
    }
}

int main ()
{
 int no_of_elements, element_no, element_value;
  Node *root = NULL;        
 
 scanf(" %d", &no_of_elements);
 
 for(element_no=0; element_no<no_of_elements; element_no++)
 {
     scanf(" %d", &element_value);
     root=Create_Binary_Tree(root,element_value);
 }
 
  printf (" %d ", find_treeHeight(root));
  return 0;
}
 

 
