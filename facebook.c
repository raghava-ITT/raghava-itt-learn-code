#include<stdio.h>
#include<stdbool.h>

struct facebook
{
    int friend_popularity;
    struct facebook *link;
};

typedef struct facebook profile;

profile *HEAD=NULL, *first_friend;
int friend_count=0;


int main()
{
    int no_of_profiles, number_of_friends;
    int friends_to_delete, popularity;

    scanf(" %d", &no_of_profiles);

    for(int i=0; i<no_of_profiles; i++)
    {
        scanf(" %d %d", &number_of_friends, &friends_to_delete);
        
        add_friends(popularity,number_of_friends);
        delete_friends(number_of_friends, friends_to_delete);
        print_friends();
        friend_count=0;
    }
    
}

void add_friends(int popularity, int number_of_friends)
{
    for(int friend=0; friend<number_of_friends; friend++)
    {
        scanf(" %d ",&popularity);
        add_friend_to_list(popularity, number_of_friends);
    }
}

void delete_friends(int number_of_friends, int friends_to_remove)
{
    for(int friend=0; friend<friends_to_remove; friend++)
        {
            delete_friend_from_list(number_of_friends);
        }
}

void print_friends()
{
    while(HEAD!=0)
    {
        printf("%d ",HEAD->friend_popularity );
        HEAD = HEAD->link;
    }
    printf("\n");
}


void add_friend_to_list(int value, int number_of_friends)
{
    profile *new_friend;
    if(HEAD == NULL)
    {
        new_friend = (profile *)malloc(sizeof(profile));
        new_friend->friend_popularity = value;
        new_friend->link = NULL;
        HEAD = new_friend;
        first_friend = HEAD;
        friend_count++;
    }
    else
    {
        new_friend = (profile *)malloc(sizeof(profile));
        new_friend->friend_popularity = value;
        new_friend->link = NULL;
        HEAD->link = new_friend;
        HEAD = new_friend;
        friend_count++;
    }
    if(friend_count==number_of_friends)
    {
        HEAD = first_friend;
    }
}

void delete_friend_from_list(int number_of_friends)
{
    bool friend_deleted = false;
    profile *current , *previous, *next ;
    current = HEAD;
    next = HEAD->link;
    for(int friend=1; friend<number_of_friends; friend++)
    {
        if((current->friend_popularity) < (next->friend_popularity))
        {
            if(current == HEAD)
            {
                HEAD = next;
                free(current);
            }
            else 
            {
                previous->link = next;
                free(current);
                current = next;
                next = next->link;
            }
            friend_deleted = true;
            break;
        }
        else
        {
            previous = current;
            current = next;
            next = next->link;
        }
    }
    if(friend_deleted == false)
    {
        previous->link = NULL;
        free(current);
    }
}






