
#include <iostream>
#define SIZE 99856
using namespace std;

class Queue
{
 public:
 int items[SIZE],front,rear;
 
 public:
 Queue()
 {
  front = -1;
  rear = -1;
 }
 
 void EnQueue (int element)
 {
  if(!((rear==SIZE-1 && front==0)||rear==front-1))
  {
   if(front==-1)
   {
    rear=0;
    front=0;
    items[rear]=element;
   }
   
   else if(rear==SIZE-1 && front!=0)
   {
    rear=0;
    items[rear]=element;
   }
   else
   {
    rear++;
    items[rear]=element;
   }
  }
 }
 
 int DeQueue ()
 {
  int element;
  if(!(front==-1))
  {
   element=items[front];
   
   if (front==rear)
   {
    rear=-1;
    front=-1;
   }
   
   else if(front==SIZE-1)
    front=0;
   
    else
    front++;
   return element;
  }
 }
};


class Spiders
{
public:
int totalSpiders , SelectedSpiders , SpiderPower , LargestPower , SpiderPowerPosition ;
int SelectedSpiderPower[316] , SelectedSpiderPosition[316] ;
Queue PowerOfSpider , InitialPositionOfSpider;

Spiders()
{  
    {
      cin>>totalSpiders>>SelectedSpiders;
      
    }
}

void EnQueueSpiderPower()
{
 for(int i=0;i<totalSpiders;i++)
    {
     cin>>SpiderPower;
     {
      PowerOfSpider.EnQueue(SpiderPower);
      InitialPositionOfSpider.EnQueue(i+1); 
     }
     
    }
}

void DeQueueSpiderPower()
{
 for(int j=0;j<SelectedSpiders;j++)
 {
  SelectedSpiderPower[j] = PowerOfSpider.DeQueue();
  SelectedSpiderPosition[j]=InitialPositionOfSpider.DeQueue();
 }
 
}


void MaxSpiderPower()
{

 LargestPower = SelectedSpiderPower[0];
 SpiderPowerPosition = 0;
 for(int j=1;j<SelectedSpiders;j++)
 {
  if(LargestPower<SelectedSpiderPower[j] && LargestPower!=SelectedSpiderPower[j])
  {
   LargestPower=SelectedSpiderPower[j];
   SpiderPowerPosition=j;
  }
  
 }
 
}


void DecrementSpiderPower()
{

 for(int j=0;j<SelectedSpiders;j++)
 {
  if(SelectedSpiderPower[j]>0)
   SelectedSpiderPower[j]--;
 }
 
}


void ReQueueSpiderPower()
{
 for(int j=0;j<SelectedSpiders;j++)
 {
 
  if(j!=SpiderPowerPosition)
  {
   PowerOfSpider.EnQueue(SelectedSpiderPower[j]);
   InitialPositionOfSpider.EnQueue(SelectedSpiderPosition[j]);
  }
  
 }
}

};

int main()
{
    Spiders spider=Spiders();

     spider.EnQueueSpiderPower();
     for(int i=0;i<spider.SelectedSpiders;i++)
     {
      spider.DeQueueSpiderPower();
      spider.MaxSpiderPower();
      spider.DecrementSpiderPower();
      spider.ReQueueSpiderPower();
      cout<<spider.SelectedSpiderPosition[spider.SpiderPowerPosition]<<" ";
     }
    
}