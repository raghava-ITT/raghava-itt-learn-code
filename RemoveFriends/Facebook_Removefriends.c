#include<stdio.h>
#include<stdlib.h>
 
struct facebook
{
    int popularity ;
    struct facebook *link;
};
 typedef struct facebook profile;
 
profile* createnode(int  popularity )
{
    profile* next_friend = (profile*) malloc(sizeof(profile));
    next_friend->popularity = popularity;
    next_friend->link = NULL;
    
    return next_friend;
}
 
profile* addfriend(profile* first_friend, profile* next_friend)
{
   if(first_friend)
   { 
       next_friend->link = first_friend;
   }
    return next_friend;
}
 
profile* deletefriend(profile* first_friend)
{
    if(first_friend == NULL)
   {
      return first_friend; 
   }
  
    profile *delete_friend = first_friend->link;
    free(first_friend);
    return delete_friend;
}
 
void printfriends(profile* head)
{
    if(head == NULL)
        return;
    printfriends(head->link);
    printf("%d ", head->popularity);
}

int  add_and_delete_friends(int no_of_profiles)
{
    int no_Of_friends, friends_to_delete;
    int friend_no, popularity, deleteCounter;
    
    profile* first_friend = NULL, *next_friend;
    
     while(no_of_profiles--)
   {
        scanf("%d %d", &no_Of_friends, &friends_to_delete);
        deleteCounter = friends_to_delete;
        first_friend = NULL;
        
        for(friend_no=0; friend_no<no_Of_friends; friend_no++)
        {
            scanf("%d", &popularity);
            next_friend = createnode(popularity);
            
             while(first_friend!=NULL && first_friend->popularity < popularity && deleteCounter)
             {
                    deleteCounter--;
                    first_friend = deletefriend(first_friend);
             }
                first_friend = addfriend(first_friend, next_friend);
        }
        printfriends(first_friend);
        printf("\n");
    }
    
    
}
 
int main()
{
    int no_of_profiles;
    
    scanf("%d", &no_of_profiles);
   
    add_and_delete_friends(no_of_profiles);
    return 0;
}