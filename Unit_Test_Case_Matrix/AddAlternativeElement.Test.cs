﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace AddAlternativeElement
{
    class TestAddAlternativeElement
    {
        [Test]
        public void Test_IsValidInput(int num)
        {
            //Arrange
            AddAlternativeElement addMatrix = new AddAlternativeElement();
            bool expected = true;

            //Act
            bool isValid = addMatrix.IsValidInput(6);

            //Assert
            Assert.AreEqual(expected, isValid);
        }

        [Test]
        public void Test_ElementsCount(int[] matrix_array)
        {
            //Arrange
            AddAlternativeElement addMatrix = new AddAlternativeElement();
            int expected = 6;

            //Act
            int numberofElements = addMatrix.CountOfElemets(matrix_array);

            //Assert
            Assert.AreEqual(expected, numberofElements);
        }

        [Test]
        public void Test_AddOddIndex(int[] matrix_array)
        {
            //Arrange
            AddAlternativeElement addMatrix = new AddAlternativeElement();
            int expected = 20;

            //Act
            int oddSum = addMatrix.OddIndexSum(matrix_array);

            //Assert
            Assert.AreEqual(expected, oddSum);
        }

        [Test]
        public void Test_EvenIndex(int[] matrix_array)
        {
            //Arrange
            AddAlternativeElement addMatrix = new AddAlternativeElement();
            int expected = 30;

            //Act
            int isValid = addMatrix.EvenIndexSum(matrix_array);

            //Assert
            Assert.AreEqual(expected, isValid);
        }
    }
}
