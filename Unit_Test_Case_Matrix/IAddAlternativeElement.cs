﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddAlternativeElement
{
    interface IAddAlternativeElement
    {
        bool IsValidInput(int num);
        int ElementsCount(int[] matrix_array);
        int AddOddIndex(int[] matrix_array);
        int AddEvenIndex(int[] matrix_array);
    }
}
