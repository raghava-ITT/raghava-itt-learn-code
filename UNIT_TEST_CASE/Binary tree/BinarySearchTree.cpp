using System;
class BinarySearchTree
{
 public class Node
 {
 public int Value;
 public Node LeftChild = null;
 public Node RightChild = null;

 public Node(int Value)
 {
 this.Value = Value;
 this.LeftChild = null;
 this.RightChild = null;
 }
 }

 public void insertNode(ref Node root, int Value)
 {
 if (root == null)
 {
 root = new Node(Value);
 }

 else if (root.Value >= Value && root.LeftChild == null)
 {
 root.LeftChild = new Node(Value);
 }

 else if (root.Value >= Value && root.LeftChild != null)
 {
 insertNode(ref root.LeftChild, Value);
 }

 else if (root.Value < Value && root.RightChild == null)
 {
 root.RightChild = new Node(Value);
 }

 else if (root.Value < Value && root.RightChild != null)
 {
 insertNode(ref root.RightChild, Value);
 }
 }

 public int getHeightofTree(Node root)
 {
 if (root == null)
 return 0;

 else
 {
 int lHeight = getHeightofTree(root.LeftChild);
 int rHeight = getHeightofTree(root.RightChild);
 return (Math.Max(lHeight, rHeight) + 1);
 }
 }

 public bool SearchElement_Rec(int element, Node root)
 {
 Node current = root;

 if (current == null)
 return false;

 if (element == current.Value)
 return true;

 if (element < current.Value)
 return this.SearchElement_Rec(element, current.LeftChild);

 else
 return this.SearchElement_Rec(element, current.RightChild);
 }

 static void Main(string[] args)
 {
 Node root = null;
 int count = int.Parse(Console.ReadLine());
 string[] Values = Console.ReadLine().Split(' ');
 BinarySearchTree BST = new BinarySearchTree();

 foreach (string str in Values)
 {
 BST.insertNode(ref root, int.Parse(str));
 }
 int hightofTree = BST.getHeightofTree(root);
 Console.Write(hightofTree);
 }
}




wget --post-data=\'' . $json . '\' --header="Accept: application/json, text/plain, */*" --header="Content-Type: application/json;charset=utf-8" http://localhost:12350/api/settings\


 --header "Accept: application/json" --header "Content-Type: application/json"


curl -S -X POST  -H "Accept: application/json"  -d  "registrationKey=ABCD_EFGH_IJKL"  "http://localhost:12350/api/settings"