using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace BinarySearchTreeTest
{
 class Program
 {
 [Test]
 public void insertNodeTest(BinarySearchTree.Node root)
 {
 BinarySearchTree BST = new BinarySearchTree();

 bool expected = true;
 BST.insertNode(ref root, 10);

 bool actual = BST.SearchElement_Rec(10, root);
 Assert.AreEqual(expected, actual);
 } 

 [Test]
 public void getHeightofTreeatZeroPositionTest()
 {
 BinarySearchTree BST = new BinarySearchTree();
 BinarySearchTree.Node node0 = null;
 int expected = 0;

 int actual = BST.getHeightofTree(node0);
 Assert.AreEqual(expected, actual);

 BinarySearchTree.Node node = new BinarySearchTree.Node(10);
 }

 [Test]
 public void getHeightofTreeTest()
 {
 BinarySearchTree BST = new BinarySearchTree();
 BinarySearchTree.Node root = null;

 BST.insertNode(ref root, 1);
 BST.insertNode(ref root, 2);
 BST.insertNode(ref root, 3);
 BST.insertNode(ref root, 4);
 int expected = 4;

 int actual = BST.getHeightofTree(root);
 Assert.AreEqual(expected, actual);
 }

 [Test]
 public void SearchElementatZeroPosition_RecTest()
 {
 BinarySearchTree.Node root = null;
 BinarySearchTree BST = new BinarySearchTree();

 bool expected = false;
 bool actual = BST.SearchElement_Rec(10, root);
 Assert.AreEqual(expected, actual);
 }

 [Test]
 public void SearchElement_RecTest()
 {
 BinarySearchTree BST = new BinarySearchTree();
 BinarySearchTree.Node root = null;

 BST.insertNode(ref root, 1);
 BST.insertNode(ref root, 2);
 BST.insertNode(ref root, 3);
 BST.insertNode(ref root, 4);

 bool expected = true;
 bool actual = BST.SearchElement_Rec(4, root);
 Assert.AreEqual(expected, actual);
 }
 }
}





