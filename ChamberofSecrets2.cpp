#include<iostream>
using namespace std;
#define MINSPIDERS 1
#define MAXSPIDERS 316
#define QueueSize 99856

class CircularQueue
{
    private: int QueueItems[QueueSize],front,rear;
    public: 
    CircularQueue()
    {
        front = -1;
        rear = -1;
    }
    void EnQueue(int element)
    {
        if(!((rear==QueueSize-1 && front==0)||rear==front-1))
        {
            if(front==-1)
            {
                rear=0;
                front=0;
                QueueItems[rear]=element;
            }
           else if(rear==QueueSize-1 && front!=0)
            {
                rear=0;
                QueueItems[rear]=element;
            }
            else
            {
                rear++;
                QueueItems[rear]=element;
            }
        }
    }
    int DeQueue ()
    {
        int element;
        if(!(front==-1))
        {
            element=QueueItems[front];
            if (front==rear)
            {
                rear=-1;
                front=-1;
            }
            else if(front==QueueSize-1)
                front=0;
            else
                front++;
        }
        else
        {
            element=-1;
        }
        return element;
    }
};

class Spiders: public CircularQueue
{
public:
CircularQueue EnqueueSpiderPower , Spiderposition;

void ToEnQueueSpiders(int selectedspiders)
{
    int SpiderPower;
    for(int SpiderIndex=1; SpiderIndex<=selectedspiders; SpiderIndex++)
    {
           cin>>SpiderPower;
            EnqueueSpiderPower.EnQueue(SpiderPower);
            Spiderposition.EnQueue(SpiderIndex); 
     
    }
}

void ToDeQueueSpiders(int selectedspiders,int *SpiderPowers,int *SpiderPositions,int *DequeuedSpiders)
{
    *DequeuedSpiders=0;
    int TempPower;
    for(int SpiderIndex=1;  SpiderIndex<=selectedspiders; SpiderIndex++)
    {
        TempPower=EnqueueSpiderPower.DeQueue();
        if(TempPower!=-1)
        {
          SpiderPowers[SpiderIndex] = TempPower;
          SpiderPositions[SpiderIndex] = Spiderposition.DeQueue();
           (*DequeuedSpiders)++;
        }
    }
}


void ToEnqueuePowerDecrementedSpiders(int DequeuedSpiders,int MaxPowerSpiderPosition,int *SpiderPowers,int *SpiderPositions)
{
    for(int SpiderIndex=1; SpiderIndex<=DequeuedSpiders; SpiderIndex++)
    {
        if(SpiderIndex != MaxPowerSpiderPosition)
        {
            EnqueueSpiderPower.EnQueue(SpiderPowers[SpiderIndex]);
            Spiderposition.EnQueue(SpiderPositions[SpiderIndex]);
        }
    }
}

void ToSelectMaxPoweredSpider(int DequeuedSpiders,int *MaxPowerSpider,int *MaxPowerSpiderPosition,int *SpiderPowers,int *SpiderPositions)
{
    *MaxPowerSpider = SpiderPowers[0];
    *MaxPowerSpiderPosition = 1;
    for(int Spiderindex=1; Spiderindex<=DequeuedSpiders; Spiderindex++)
    {
        if(SpiderPowers[Spiderindex] > *MaxPowerSpider && *MaxPowerSpider != SpiderPowers[Spiderindex])
        {
            *MaxPowerSpider = SpiderPowers[Spiderindex];
            *MaxPowerSpiderPosition = Spiderindex;
        }
    }
}


void ToDecrementSpiderPower(int DequeuedSpiders,int MaxPowerSpiderPosition,int *SpiderPowers)
{
    for(int SpiderIndex=1; SpiderIndex<=DequeuedSpiders; SpiderIndex++)
    {
        if(SpiderPowers[SpiderIndex]>0 && SpiderIndex!=MaxPowerSpiderPosition)
           {
            SpiderPowers[SpiderIndex]--;
           }
    }
}


};

int main()
{
    int totalspiders, selectedspiders;
    int DequeuedSpiders=0;
    int MaxPowerSpider, MaxPowerSpiderPosition;
    int SpiderPowers[MAXSPIDERS] ,SpiderPositions[MAXSPIDERS] ;
    
    cin>>totalspiders>>selectedspiders;
    
    Spiders spider;
   
        spider.ToEnQueueSpiders(totalspiders);
        for(int spider_no=1; spider_no<=selectedspiders; spider_no++)
        {
           spider.ToDeQueueSpiders(selectedspiders,SpiderPowers,SpiderPositions,&DequeuedSpiders);
           spider.ToSelectMaxPoweredSpider(DequeuedSpiders,&MaxPowerSpider,&MaxPowerSpiderPosition,SpiderPowers,SpiderPositions);
           spider.ToDecrementSpiderPower(DequeuedSpiders,MaxPowerSpiderPosition,SpiderPowers);
           spider.ToEnqueuePowerDecrementedSpiders(DequeuedSpiders,MaxPowerSpiderPosition,SpiderPowers,SpiderPositions);
           cout<<SpiderPositions[MaxPowerSpiderPosition]<<" ";
        }
   
}