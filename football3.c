#include <stdio.h>
    
int player_number[2], player_id;         /*player id and array of size two which contains 
                                          player id who has the ball and who passed it.*/
  const int MATCH_RANGE=100;
  const int PASS_RANGE=100000;
  const int ID_RANGE=1000000;                                          

int validateinput(int input,int MAX_VALUE)
{
    const  int  MIN_VALUE=1;
    
    if(input < MIN_VALUE||input > MAX_VALUE)     //returns -1 if input is out of range.
      {
          return -1;
      }
      
    else 
     {
         return input;                         //if valid input ,returns input value.
     }
}

int passing_forward(int player_id )
{
     scanf(" %d",&player_id);                   //reading integer value for player id.
    
     if(player_id!=-1)
     {
     player_id=validateinput(player_id,ID_RANGE);

     player_number[0]=player_number[1];          //storing player id at index zero from index one.
                  
     player_number[1]=player_id;                  //storing new player id at index one.
     }
}

int passing_backward()
{
    int temp;
    
    temp=player_number[1];                         //swapping of player id's at index one and zero. 
    
    player_number[1]=player_number[0];
    
    player_number[0]=temp;
    
}

int playing(int no_of_passes)
{
    const char FORWARD_PASS='P';
    
    const char BACKWARD_PASS='B';
 
    char pass_type=NULL;
    
    while(no_of_passes!=0)
    {
       scanf(" %c",&pass_type);               //reading a character for pass_type
    
       if(pass_type==FORWARD_PASS)            //comparing pass_type with constant forward_pass.
      {
       passing_forward(player_id);             //calling passing_forward function with playerd_id argument.
       
       --no_of_passes;
      }
    
      else if(pass_type==BACKWARD_PASS)           //comparing pass_type with constant backward_pass
      {
        passing_backward();                       //calling passing_backward function.
       
        --no_of_passes;
      }
    
    }
    printf(" Player %d \n",player_number[1]);         //writing the final player number who has the ball after all passes.
    
}

int main()
{
  int match_number, no_of_passes;
 
  scanf("%d",&match_number);                              //reading number of matches to be played.
   
  match_number=validateinput(match_number,MATCH_RANGE);
  
   while(match_number!=0)                                  //untill match number equals to zero.
  {
    scanf(" %d %d",&no_of_passes,&player_id);              //reading no of passess and initial player id.

    no_of_passes= validateinput(no_of_passes,PASS_RANGE);  //calling validateinput function for checking input range
    
    if(no_of_passes!=-1)
    {
    player_id= validateinput(player_id,ID_RANGE);
   
    if(no_of_passes!=1)
    {
    player_number[0]=player_id;                             // storing initial playerid in array at index 0;
    
    playing(no_of_passes);                                 //calling playing function with no_of_passes argument.
   
    }  
    //no_of_passes--;
    }
    match_number--;
  }
  
  
}


