#include<stdio.h>
    
int player_number[2], player_id;            /*player id and array of size two which contains 
                                            player id who has the ball and who passed the ball.*/

int  passing_forward(int player_id )
{
     scanf(" %d",&player_id);                //reading integer value for player id.
     
     player_number[0]=player_number[1];      //copying player id into index zero from index one .
                  
     player_number[1]=player_id;              //storing new player id at index one.
}

int passing_backward()
{
    int temp;
    
    temp=player_number[1];                    //swapping of player id's at index one and index zero. 
    
    player_number[1]=player_number[0];
    
    player_number[0]=temp;
    
}

int playing(int no_of_passes)
{
    const char FORWARD_PASS='P';
    
    const char BACKWARD_PASS='B';
    
    char pass_type=NULL;
    
    while(no_of_passes!=0)
    {
     scanf(" %c",&pass_type);                    //reading a character for pass_type
    
     if(pass_type==FORWARD_PASS)                 //comparing pass_type with constant forward_pass.
     {     
     passing_forward(player_id);                 //calling passing_forward function with playerd_id argument.
       
     --no_of_passes;
     }
    
     else if(pass_type==BACKWARD_PASS)            //comparing pass_type with constant backward_pass
     {
     passing_backward();                          //calling passing_backward function.
       
     --no_of_passes;
     }
    
    }
    printf(" Player %d \n",player_number[1]);     //writing the final player number who has the ball after all passes.
    
}

int main()
{
  int match_number, no_of_passes;
  
  scanf("%d",&match_number);                     //reading number of matches to be played.
  
   while(match_number!=0)                        //untill match number equals to zero.
  {
    scanf(" %d %d",&no_of_passes,&player_id);    //reading no of passess and initial player id.

    player_number[0]=player_id;                  // storing initial playerid in array at index 0;
    
    playing(no_of_passes);                       //calling playing function with no_of_passes argument.
      
    match_number--;
  }
  
}


