#include<stdio.h>
#include<string.h>
#include<stdlib.h>
 
struct AddPerson
{
	int name;
	struct AddPerson* next;
};
 
struct sportLikes
{
	char sportName[11];
	int count;
	struct sportLikes * next;
};
 
struct AddPerson ** hashname;
struct sportLikes **hashsportName;
int size;

unsigned long GetHashcode(unsigned char *str)
{
	unsigned long hashSize = 5381;
	int c;
 
	while (c = *str++)
		hashSize = ((hashSize << 5) + hashSize) + c; /* hashSize * 33 + c */
 
	return hashSize;
}
 
void StartTheSurvey(int noOfPeople)
{
	char person_name[21], sport_name[21], popular_sport[21];
	int  football_count = 0, k, max = 0;
	while (noOfPeople--)
	{
		scanf("%s %s", person_name, sport_name);
		if (!IsPerson_Has_Done_Survey(person_name))
		{
			k = hash_it(sport_name);
			if (k>max || (k == max && (strcmp(sport_name, popular_sport)<0)))
			{
				max = k;
				strcpy(popular_sport, sport_name);
			}
			if (strcmp(sport_name, "football") == 0)
				football_count++;
		}
	}
	printf("%s\n%d", (strcmp(popular_sport, "badminton") == 0 ? "cricket" : popular_sport), football_count);
  //  printf("%s\n%d",  popular_sport, j);
}
 
int IsPerson_Has_Done_Survey(char *person_name)
{
	unsigned long k = GetHashcode(person_name);
	int pos = k%size;
	struct AddPerson* cur = hashname[pos];
	while (cur)
	{
		if (cur->name == k)
			return 1;
		cur = cur->next;
	}
	struct AddPerson* head = (struct AddPerson*)malloc(sizeof(struct AddPerson));
	head->name = k;
	head->next = hashname[pos];
	hashname[pos] = head;
	return 0;
}
int hash_it(char* str)
{
	unsigned long k = GetHashcode(str);
	int pos = k%size;
	struct sportLikes* cur = hashsportName[pos];
	while (cur)
	{
		if (strcmp(str, cur->sportName) == 0)
		{
			cur->count++;
			return cur->count;
		}
		cur = cur->next;
	}
	struct sportLikes* head = (struct sportLikes*)malloc(sizeof(struct sportLikes));
	strcpy(head->sportName, str);
	head->count = 1;
	head->next = hashsportName[pos];
	hashsportName[pos] = head;
	return 1;
}
 
int main()
{
	int no_of_people;
	//char [21];
	size = no_of_people;
	scanf("%d", &no_of_people);
	hashname = (struct nameofPerson**)malloc(size*sizeof(struct AddPerson*));
	hashsportName = (struct sportLikes**)malloc(size*sizeof(struct sportLikes*));
	StartTheSurvey(no_of_people);
	
	return 0;
}
    	
