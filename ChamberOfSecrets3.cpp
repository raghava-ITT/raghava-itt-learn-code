#include<iostream>
using namespace std;
#define MINSPIDERS 1
#define MAXSPIDERS 316
#define QueueSize 99856

int MaxSpiderPower=0, MaxSpiderPowerPosition=0;
int DequeuedSpiders=0;

class CircularQueue
{
    private: 
    int QueueItems[QueueSize],front,rear;
    public: 
    
    CircularQueue()
    {
        front = -1;
        rear = -1;
    }
    void EnQueue(int element)
    {
        if(!((rear==QueueSize-1 && front==0)||rear==front-1))
        {
            if(front==-1)
            {
                rear=0;
                front=0;
                QueueItems[rear]=element;
            }
           else if(rear==QueueSize-1 && front!=0)
            {
                rear=0;
                QueueItems[rear]=element;
            }
            else
            {
                rear++;
                QueueItems[rear]=element;
            }
        }
    }
    int DeQueue ()
    {
        int element;
        if(!(front==-1))
        {
            element=QueueItems[front];
            if (front==rear)
            {
                rear=-1;
                front=-1;
            }
            else if(front==QueueSize-1)
                front=0;
            else
                front++;
        }
        else
        {
            element=-1;
        }
        return element;
    }
};

class Spiders: public CircularQueue
{
public:
CircularQueue EnqueueSpiderPower , Spiderposition;

void ToEnQueueSpiders(int totalspiders)
{
    int SpiderPower;
    for(int SpiderIndex=1; SpiderIndex<=totalspiders; SpiderIndex++)
    {
        cin>>SpiderPower;
       
            EnqueueSpiderPower.EnQueue(SpiderPower);
            Spiderposition.EnQueue(SpiderIndex); 
     
    }
}


void ToDeQueueSpiders(int selectedspiders,int *SpiderPowersArray,int *SpiderPositionsArray)
{
    DequeuedSpiders=0;
    int TempPower;
    for(int SpiderIndex=1;  SpiderIndex<=selectedspiders; SpiderIndex++)
    {
     
        TempPower=EnqueueSpiderPower.DeQueue();
        if(TempPower!=-1)
        {
          SpiderPowersArray[SpiderIndex] = TempPower;
          SpiderPositionsArray[SpiderIndex] = Spiderposition.DeQueue();
           DequeuedSpiders++;
        }
    }
}


void ToSelectMaxPoweredSpider(int *SpiderPowersArray,int *SpiderPositionsArray)
{
    MaxSpiderPower = SpiderPowersArray[0];
    MaxSpiderPowerPosition = 1;
    for(int Spiderindex=1; Spiderindex<=DequeuedSpiders; Spiderindex++)
    {
        if(SpiderPowersArray[Spiderindex] > MaxSpiderPower)
        {
            MaxSpiderPower = SpiderPowersArray[Spiderindex];
            MaxSpiderPowerPosition = Spiderindex;
        }
    }
}

void ToDecrementSpiderPowerAndEnqueuing(int *SpiderPowersArray,int *SpiderPositionsArray)
{  
    for(int SpiderIndex=1; SpiderIndex<=DequeuedSpiders; SpiderIndex++)
    {
        if(SpiderPowersArray[SpiderIndex]>0 && SpiderIndex!=MaxSpiderPowerPosition)
        {
            SpiderPowersArray[SpiderIndex]--;    
        }          
    }
}


void ToEnqueueDecrementedPower(int *SpiderPowersArray,int *SpiderPositionsArray)
{
     for(int SpiderIndex=1; SpiderIndex<=DequeuedSpiders; SpiderIndex++)
     {
         if(SpiderIndex != MaxSpiderPowerPosition )
          {
            EnqueueSpiderPower.EnQueue(SpiderPowersArray[SpiderIndex]);
            Spiderposition.EnQueue(SpiderPositionsArray[SpiderIndex]);   
          }
     }   
}

};


int main()
{
    int totalspiders, selectedspiders;
   
    int SpiderPowersArray[MAXSPIDERS] ,SpiderPositionsArray[MAXSPIDERS] ;
    
    cin>>totalspiders>>selectedspiders;
    
    Spiders spiderobject;
   
        spiderobject.ToEnQueueSpiders(totalspiders);
        
        for(int spider_no=1; spider_no<=selectedspiders; spider_no++)
        {
           spiderobject.ToDeQueueSpiders(selectedspiders,SpiderPowersArray,SpiderPositionsArray);
         
           spiderobject.ToSelectMaxPoweredSpider(SpiderPowersArray,SpiderPositionsArray);
          
           spiderobject.ToDecrementSpiderPowerAndEnqueuing(SpiderPowersArray,SpiderPositionsArray);
          
           spiderobject.ToEnqueueDecrementedPower(SpiderPowersArray,SpiderPositionsArray);
           
           cout<<SpiderPositionsArray[MaxSpiderPowerPosition]<<" ";
        }
}